FROM bitnami/pytorch:latest
ADD req2.txt .
RUN pip install -r req2.txt
WORKDIR /usr/src/predict_fnal
ADD predict.py .
ADD dogs_cats_model.pth .
CMD ["python", "predict.py"]